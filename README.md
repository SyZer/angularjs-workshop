# AngularJS Workshop

## Introduction

This workshop guides you trough [AngularJS](http://angularjs.org/), its components and how to use them. This guide is only tested on OSX 10.7.5 with XCode 4.3.2.

## Installing dependencies

 Unfortunately there are some issues with Yeoman on OSX (2013-04-29), so please follow the steps below carefully.

1. Install XCode.
2. Install command line tools from XCode: Start XCode -> Preferences (Cmd + ,) -> Downloads -> Click Install on "Command Line Tools"
3. Install [NodeJS](http://nodejs.org/) (tested with 0.10.5), or upgrade existing installation with: ```sudo npm install -g n && sudo n latest```. Note, yeoman has issues (#524) with nodejs installed from brew, so please uninstall and download latest from [nodejs.org](http://nodejs.org/).
4. Before you install yeoman, run ```node -v && npm -v``` to verify up to date version, the output should be (or newer):
```
v0.10.5
1.2.18
```
5. Install [Yeoman](http://yeoman.io/), run: 
```sudo npm install -g yo``` (tested with 1.0 BETA) (ignore errors about that node 0.8.x was wanted).
6. Install [Grunt](http://gruntjs.com/) and [Bower](http://bower.io/) with: 
```sudo npm install -g grunt-cli bower```.
7. Install angular generators: 
```sudo npm install -g generator-angular generator-karma  # install generators```

## Generating an AngularJS project with Yeoman

1. Create a directory where your new app will be living, and cd into it.
2. Create an AngularJS app with E2E-tests and dev setup:
```
yo angular                     # scaffold out a AngularJS project (default is good, except Compass [less setup])
bower install angular-ui       # install a dependency for your project from Bower
grunt test --force                   # test your app
grunt server --force            # preview your app
grunt                          # build the application for deployment
```
## Configure IntelliJ/WebStorm

1. Settings -> Plugins -> Browse repositories -> Right click on NodeJS and choose "Download and install", also install the AngularJS plugin. Restart IntelliJ/WebStorm.
2. File -> Create new project -> Create project from existing source -> Choose directory you generated your angular app -> exclude node_modules
3. Create a new Node.js runner configuration for running tests:
```
Path to Node: /usr/local/bin/node
Working Directory: /Users/staale/code/angularjs-workshop
Path to Node App JS File: /usr/local/bin/grunt
Application parameters: test --force
```
3. Create a new Node.js runner configuration for running application:
```
Path to Node: /usr/local/bin/node
Working Directory: /Users/staale/code/angularjs-workshop
Path to Node App JS File: /usr/local/bin/grunt
Application parameters: server --force
```



npm install grunt --save-dev
npm install node-markdown --save-dev
npm install grunt-karma grunt-contrib-copy grunt-contrib-concat --save-dev
npm install grunt-contrib-watch grunt-contrib-jshint grunt-contrib-uglify --save-dev

➜  angular-placeholders git:(master) ✗ grunt -version
grunt-cli v0.1.7
grunt v0.4.1


# Tasks

## Create your first app

The ng-app initializes Angular, so no ng-app, no Angular.

Create an index.html, include angular.js and add a `<div ng-app="AngularWorkshopApp"></div>`.

## Two-way binding

Clone: 

Start server:

Open exising controller and add a model to $scope.
Create an input field that is prepopulated with the model value. When you change the value in the input field, the model in the controller should be updated.

## Template

Keep in mind that all ng-* attributes are Angular Directives.

### Add this array to your model, print all elements in the array.

var people = [
{'name': 'Fred', 'age': 70, 'alive' : true},
{'name': 'Jack', 'age': 10, 'alive' : true},
{'name': 'Lisa', 'age': 30, 'alive' : true},
{'name': 'John', 'age': 90, 'alive' : false},
 ];

### They should be sorted

### Only show people that's alive





